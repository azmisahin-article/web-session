# Oturum yönetimi ve JWT örneği

Dockerfile'ı kullanarak Docker imajınızı oluşturun. Dockerfile ile aynı dizindeyseniz, aşağıdaki komutu kullanabilirsiniz:

```shell
docker build -t web-session-nodejs:latest .
```

Bu komut, web-session adlı bir Docker imajı oluşturur.

Şimdi, bu Docker imajını kullanarak Docker konteynerlerini başlatmak için kullanabilirsiniz. PM2 ile başlattığınız gibi, her uygulama için ayrı bir port ve isim belirleyebilirsiniz:

```shell
docker run -d -p 3001:3000 --name app-1 web-session-nodejs:latest
docker run -d -p 3002:3000 --name app-2 web-session-nodejs:latest
docker run -d -p 3003:3000 --name app-3 web-session-nodejs:latest
```

Yukarıdaki komutlar, her bir Docker konteynerini ayrı bir portta çalıştırır ve verilen isimleri kullanır. Bu sayede Express.js uygulamanızı Docker konteynerlerinde çalıştırabilirsiniz.

# Register
```shell
docker login registry.gitlab.com
docker tag web-session-nodejs:latest registry.gitlab.com/azmisahin-article/web-session/web-session-nodejs:latest
docker push registry.gitlab.com/azmisahin-article/web-session/web-session-nodejs:latest
```

## Usage
```YML
version: '3'
services:
  web:
    image: registry.gitlab.com/azmisahin-article/web-session/web-session-nodejs
    ports:
      - "80:3000"
```

# Test

http://localhost/api/login

http://localhost/api/logout

http://localhost/api/profile

http://localhost/api/jwt-login

http://localhost/api/jwt-profile