# Node.js temel imajını kullanın
FROM node:14

# Uygulamanızın çalışacağı dizini oluşturun
WORKDIR /app/web-session

# Uygulama bağımlılıklarını kopyalayın ve yükleyin
COPY package*.json ./
RUN npm install

# Uygulama kodunu kopyalayın
COPY . .

# Uygulamayı çalıştırın
CMD ["node", "index.js"]