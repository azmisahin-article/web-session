// web-session

/**
 * Oturum Yönetimi
 * @file index.js
 */

// Express uygulaması ve rotaları oluştur
const express = require('express');
const session = require('express-session');
const jwt = require('jsonwebtoken');
const app = express();


// Express.js Oturum Yönetimi
app.use(
    session({
        // Oturum verilerini şifrelemek için kullanılır
        secret: 'gizli_bir_anahtar',
        resave: false,
        saveUninitialized: true,
        // HTTPS kullanmıyorsanız "false" olmalıdır
        cookie: { secure: false },
    })
);

// Ortak footer HTML içeriği
// Gelen isteğin başlıklarını alın ve footer'a yazdırın
const commonFooter = function (req) {

    // Sunucu adresini alma
    const serverIpAddress = `${req.hostname}:${server.address().address}:${server.address().port}`;
    // İstemci IP adresi
    const clientIpAddress = `${req.ip}:${req.socket.remotePort}`;

    const result = { serverIpAddress, clientIpAddress }

    console.log(`Sistem : Common Footer`, result);

    return `
    <footer>
        <p>Server Ip Address: ${serverIpAddress}</p>
        <p>Client Ip Address: ${clientIpAddress}</p>
        <p>Host Header: ${req.headers.host}</p>
        <p>X-Real-IP Header: ${req.headers['x-real-ip']}</p>
        <p>Application Port : ${port}</p>
        <p>Request URL: ${req.originalUrl}</p>
    </footer>
  `;
}

// Middleware: Her yanıtın sonuna footer ekler
app.use((req, res, next) => {
    // Yanıtın sonuna footer ekleyin
    const originalSend = res.send;
    res.send = function (...args) {
        args[0] += commonFooter(req);
        originalSend.apply(res, args);
    };
    next();
});

// JSON Web Tokens (JWT) için anahtar
const secretKey = 'gizli_bir_jwt_anahtari';

// Oturumla ilgili işlemler
app.get('/:dynamicRoute/login', (req, res) => {
    req.session.user = 'kullanici_adi';
    res.send('Giriş yapıldı.');
});

app.get('/:dynamicRoute/logout', (req, res) => {
    req.session.destroy();
    res.send('Çıkış yapıldı.');
});

app.get('/:dynamicRoute/profile', (req, res) => {
    if (req.session.user) {
        res.send('Kullanıcı profiliniz: ' + req.session.user);
    } else {
        res.send('Oturum açmadınız.');
    }
});

// JWT ile oturum koruma
app.get('/:dynamicRoute/jwt-login', (req, res) => {
    const token = jwt.sign({ username: 'kullanici_adi' }, secretKey);
    res.send('Token: ' + token);
});

app.get('/:dynamicRoute/jwt-profile', (req, res) => {
    const token = req.headers.authorization;
    if (token) {
        jwt.verify(token, secretKey, (err, decoded) => {
            if (err) {
                res.send('Token geçerli değil.');
            } else {
                res.send('Kullanıcı profiliniz: ' + decoded.username);
            }
        });
    } else {
        res.send('Token eksik.');
    }
});


// Api sunucu

// Üçüncü argüman (index 2) port numarasını içerir.
const portArg = process.argv[2];

// Eğer argüman yoksa varsayılan olarak ortam değişkeni kullan.
const portNumber = portArg || process.env.PORT;

// Eğer herhangi bir tanım yok ise, varsayılan 3000 portu ile başla.
const port = portNumber || 3000;
const server = app.listen(port, '0.0.0.0', () => {
    console.log(`Sistem : Sunucu ${port} portunda çalışıyor.`);
});
